# Social Network for community - Front end

## Technologies stack

- ReactJS
- Typescript
- Tailwind CSS

## Command reminder

-   Open terminal command windows: Ctrl + `
-   Create react app: npx create-react-app social_network_frontend --template typescript
-   Setup with git:
    -   git init
    -   git add .
    -   git commit -m "Initial commit"
    -   git config user.name "box100101"
    -   git config user.email "duytuong100101@gmail.com"
    -   git remote add origin https://gitlab.com/box100101/social_network_frontend.git
    -   git push --set-upstream origin master